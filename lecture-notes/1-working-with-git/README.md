# Chapter 1. Working with git

In this module you will learn the basics of Git and get acquainted with GitLab (the website you're currently perusing).

Git is a so-called version control system (VCS). It is, simply put, a Microsoft Word Track Changes for source code: it keeps track of every single change made to a piece of code i.e. which lines are added/delete by whom, when and why. Changes are bundled in commits and each commit is stored on disk alongside a message describing the applied changes. Past commits are browseable which makes it easy to follow the implementation process from start to finish.

GitLab is an online service for collaborating on Git-versioned software projects. You can share your code, others can browse, download and keep it in sync by `pull`ing it regularly, and easily contribute additions/fixes through `merge requests`. Issues, bugs and feature requests are submitted in the `issue tracker` where they can be discussed and `closed` once fixed/implemented.

Git (in combination with GitLab) enable an open, transparent and traceable, and collaborative software development workflow. A perfect match for today's call for open data and open science.

The best way to come to grips with Git, the Git-based online platforms and the workflow they allow/induce is to roll up your sleeves and dive in. To fully appreciate Git's added value we believe it is best to interact with it first and reflect on it later. In this chapter you will therefore complete the exercise first and only then move on to the bigger picture. Curious to know why you should bother with Git? Jump straight to the Reflection section.

## Rationale

In recent years Git has become the corner stone of open source software. It sits at the basis of GitHub and GitLab both of which host thousands of open source software projects. A basic proficiency with Git and GitLab/Hub will give you access to this treasure trove of software libraries and the broader open source software ecosystem that has grown around them.

More importantly, proper use of Git (or any version control system for that matter) will greatly aid you in your software development activities. Keeping code in a VCS relieves you from manual versioning and the resulting "version hell" formed by a multitude of files named `version2-final_reviewed_final.txt`. VCSs reduce change anxiety i.e. the reluctance to change/improve/experiment with a piece of code born from the fear of braking or losing working code. Finally, the commits messages force you to divide your work in small(er) (and therefore manageable) chunks; the commit messages force you to keep the bigger piture in sight and reason about your coding/design decisions on a more abstract level.

Git's benefits become really apparent once you need to share your code and/or collaborate with others (e.g. tutors, colleagues, fellow researchers). Git's changelog keeps track of who added which functionality when and why; accepting code from others is fairly straightforward; going back to a previous snapshot in case something goes wrong is doable as well.

Git's collaborative features have become extremely popular in recent years. An increasing number of people are starting to apply the Git-based workflow to non-coding projects.

## Outcomes

- basic proficiency in interacting with Git through the command line: checking diffs and logs, and commiting files and changes
- familiarity with GitLab: browsing files, viewing a project's history and submitting issues
- basic proficiency with the Git-induced collaborative workflow: forking projects, creating merging requests and submitting issues

## Resources

### Course

|    Course type    | Course                                                            | Remarks                                       |
| ----------------- | ----------------------------------------------------------------- | --------------------------------------------- |
| Learngitbranching | [Learn Git Version Control](https://learngitbranching.js.org/)    | From Main: Introduction Sequence & Ramping Up |
|                   |                                                                   | From remote: Push & Pull -- Git Remotes!      |

In this course you will learn how to

- create repositories, commit files and track changes
- keep track of your development progress by inspecting diffs
- trash changes and revert to previous commits
- create, remove and use branches

### Reference

Chapters 1 and 2 from the the [Pro Git](https://git-scm.com/book/en/v2) book.

## Lecture notes

The Learngitbranching course taught you the basics of Git. In these lecture notes we will explore the collaborative features in more detail.

### Git

#### Basic commands

You can find a list of the basic Git commands in the [Reference](https://git-scm.com/docs) section of the [Pro Git](https://git-scm.com/book/en/v2) book. Have a look at GitLab's [Git Cheat Sheet](https://about.gitlab.com/images/press/git-cheat-sheet.pdf) as well.

##### Branches

Git's branching mechanism is a powerful feature that helps you manage the evolution of your code base in an elegant and stress-free manner.

Branches allow you to easily create independent copies of your code that you can change without affecting the main copy. This is beneficial when e.g. implementing new features and fixing bugs as you can edit the branched code as you see fit without having to worry about losing or breaking well-behaving code the main branch. Changes applied in a branch stay in that branch until they are merged back into the main branch or a different branch altogether.

Branches are super useful when implementing new features, fixing bugs or [refactoring](https://en.wikipedia.org/wiki/Code_refactoring) (i.e. improving) your code. Changing code in any way carries with it a non-neglible risk of breaking it, introducing (new) bugs or losing functioning code. That is why, in practice, you will try to hold on the working code until you are satisfied that the new code works as expected [1]. You can either

- copy/paste the file you are planning to update and give it a different name
- comment the old code and keep it close to the new code

Both scenarios are undesirable as both require a large amount of manual bookkeeping. In the first case you will have to delete a file, while in the second case you will have to delete code once done. Both approaches generate fault sensitive extra work and/but also, more importantly, make the whole editing/improvement process stressful (what if you make a mistake and lose the old code?). This in turn makes you reluctant to change the code eventually resulting in a form of writer's block.

Branches remove this dynamic almost completely. Whenever a new feature is to be built one creates a new branch thereby hiding the working code from view and automatically preventing any accidental deletions, manual book keping or versioning of files. If the new code gets to stay it is merged in the main branch (alongside a crystal clear record of the changes as recorded in the commits and diffd) else it is thrown away (by way of deleting the branch) and work resumes on the main branch.

To finalise, branches are great and you should use them as often as possible. Read more about [Branching and Merging](https://git-scm.com/book/en/v2/Git-Branching-Basic-Branching-and-Merging) in the Pro Git Book.

[1] Actually, one would like to hold on the old code indefinitely in case the new code breaks further down the road or turns out to be the wrong solution, has worse performance, etc.

#### Collaborative commands

Git's collaborative functionality is contained in its ability to retrieve and deliver code changes from/to different
offline and, more importantly, online repositories as hosted on GitLab/GitHub or private servers.

Three commands form the basis of Git's collaboration tools.

- **clone** - create a copy of a repository and retain a link to the root repository (i.e the repository it is copied from).
- **pull** - retrieve new commits from the root repository and merge them into the cloned repository.
- **push** - send new commits to the root repository.

Note: the root directory is also known as the remote repository.

Git's collaborative workflow is initiated by cloning a repository with the `clone` command. Git fetches the repository from the remote location and stores it on disk while retaining a link to the remote repository.

`push` is used to send newly added and committed changes to the remote repository.

`pull` is used to fetch and merge new commits from the remote repository to the local repository.

The following snippet shows the whole process from start to finish. Lines that start with `#` are comments and are not executed by the terminal.

    # clone repository
    git clone https://gitlab.com/repository/x/y/z.git

    # edit files and commit changes
    nano some_file.txt
    git add some_file.txt
    git commit

    # push changes to the remote repository
    git push

    # the remote repository is updated by collaborators
    git pull

    # collaborators' changes are merged into local repository

#### Git Installation

To use Git on your machine you need to install it.

- Go to https://git-scm.com/downloads and download for your OS.
- Run the installer (you can accept the default options)

When you need to use git commands open a `Git Bash` console (`Start` -> `Git` -> `Git Bash`) and use the commands in this console.

### GitLab

GitLab is an online service for hosting and collaborating on Git-versioned software projects (i.e. Git repositories). You can share your code, others can browse, download and `pull` it, and easily contribute additions/fixes through `merge requests`. Issues, bugs and feature requests are submitted in the `issue tracker` where they can be discussed and `closed` once fixed/implemented.

#### Basic features

- **Files** - the [files](https://gitlab.com/unigis/programming-for-gis/tree/main) page lists the files tracked by this Git project. Click on a folder or file to open it.
- **Issues** - the [issue tracker](https://gitlab.com/unigis/programming-for-gis/issues) keeps track of the submitted issues. An issue may be anything: a bug report, feature request, remark about the implementation of a feature, question about the usage of module, etc. An issue has a title, description and a label. Each issue has its own comments thread. Once an issue is resolved it is closed and archived for posterity.
- **Commits** - the [commits](https://gitlab.com/unigis/programming-for-gis/commits/main) page lists recent commits and thereby forms the **history** of a project.

#### Collaborative features

The main collaborative GitLab features are the so-called `forks` and `merge requests`.

- **Fork** - a standalone copy (i.e. clone) of a repository that "knows" where it was cloned from i.e. it retains a link to its root repository. A fork is basically an execution of the Git `clone` command on GitLab.
- **Merge request** - a request to merge the additions in a cloned repository into the root repository i.e. the repository it was cloned from.

`Fork`s and `merge requests` form the basis of GitLab's collaborative workflow which is similar to Git's (see above) and works as follows: suppose Sarah wants to add some functionality to Linda's code which is hosted on GitLab. Sarah needs to

1. fork Linda's repository. This will create a standalone copy of Linda's repository in Sarah's GitLab account.
2. clone the forked repository to her local machine so she can edit the code in her favourite editor
3. commit the changes and push them to her GitLab repository
4. initiate a `merge request` i.e. send the changes to Linda and ask her to merge them in her own code.

After inspecting the `merge request` Linda decides whether to accept Sarah's code or reject it.

This is all a bit abstract, isn't it? Let us see how it works in practice.

## Exercise

In this exercise you are asked to add your name to the `data/students.csv` file in this chapter through the Git/GitLab collaborative workflow described above. In other words you will

- fork this repository (to your GitLab account)
- clone it to your machine
- add your name to `students.csv`
- push the changes back to your GitLab account
- initiate a merge request

Figure 1. shows this process in a graphical manner for a repository with a single branch called `main`.

![](images/GitLab_workflow.png)

_Figure 1. We start by forking `unigis/programming-for-gis` (step 1). From our machine we issue a `git clone` command directed at `student/programming-for-gis` (step 2). After we `add`and `commit` some code to our repository on our machine we `push` it to our GitLab project (step 3) and initiate a `Merge request` (step 4) through GitLab's interface. If the merge request is accepted `print "Hello, world!"` is added to `unigis/programming-for-gis`. If for whatever reason the merge request is not up to scratch the owner of the receiving repository can reject it or provide feedback what needs to be improved/changed._

Please find a [step-by-step explanation of the fork-clone-push workflow](https://docs.google.com/presentation/d/1krhpmwLRxrVUeONlVJG0PbsNy_3VBqaIAsCLHP0AOws/edit#slide=id.g2dfd4355b6_0_376) on Google Slides. Refer to the speaker notes at the bottom of the screen for each slide's story.

### 1. Fork this project

The first step is to copy this repository to your GitLab account i.e. you need to `fork` this project.

- go to the main page of this repository: scroll up on this page and click `Project` in the top menu
- click the `Fork` button (which is located right of the Star button just below the project's title)
- choose your account as the fork destination
- wait for the forking to complete... you have now forked `programming-for-gis`, congratulations!
- verify that you are looking at the fork:
  - check whether you can spot the `Forked from unigis` text just below the project's title
  - note the fork's URL, you will need it later. It should be `https://gitlab.com/<your_username>/programming-for-gis`

**Note**: you can **safely ignore** the orange banner informing you about an **SSH key**. You do not need it at this stage as you will use your username/password to interact with your fork.

### 2. Clone your fork to your PC

Next, we need to get the forked repository on our computer so we can edit it. A Git Bash console and `clone` your newly forked project as follows

    cd C:/path/to/desired/location
    git clone https://gitlab.com/<your_username>/programming-for-gis
    cd programming-for-gis

Note that

1. you need to replace `<your_username>` with your GitLab username.
2. Git will ask for your GitLab username and password. For security reasons Bash will _not print anything_ on the `password` line (not even a bunch of **\***). Everything works fine, though, just enter your password and hit `Enter`.

The `programming-for-gis` folder now contains a clone of your GitLab repository i.e. a copy that contains a link to its _origin_ on GitLab. We can inspect the presence of this link with:

    git remote -v

    origin  https://gitlab.com/<your_username>/programming-for-gis.git (fetch)
    origin  https://gitlab.com/<your_username>/programming-for-gis.git (push)

### 3. Add your name to students.csv

`students.csv` is s text file that stores the names of students who took this course alongside their attendance year. Note that the name and year are separated with a comma (hence the extension Comma Separated Value). You will need a text editor to edit this file.

Append your name and year of attendance at the end of the file and save the file.

### 4. Commit and push the changes to GitLab

Next step is to observe your addition in Git, commit it and push it to GitLab.

Before we can `commit` our changes, however, we need to tell Git who we are and what our email is

    git config --global user.name
    git config --global user.email

Enter your real name i.e. John Doe and preferably the email you used to register for GitLab. Git will attach this information to your commits so that it is always known who made the changes.

Now we are ready to push to our GitLab repository.

    # verify that Git is aware of the changes
    git status

    # inspect the changes
    git diff lecture-notes/1-working-with-git/data/students.csv

    # add and commit the changes
    git add lecture-notes/1-working-with-git/data/students.csv
    git commit -m 'append name and attendance year to students.csv'

    # push the changes to origin on GitLab
    git push

The `status` command gives you an overview of the changed files, `diff` prints the changes, `add` and `commit` create a new commit and `push` sends it to your GitLab repository.

### 5. Initiate a merge request

You have now edited `students.csv` and pushed it to your GitLab account. You are almost there! Next step is to push your changes to the root `programming-for-gis` repository. In Gitlab this is done through a `merge request`.

You initiate a `merge request` from the source repository i.e. the repository that is sending the changes. In our case this is the repository in your GitLab account.

- go to your project's GitLab page
- verify that the push succeeded: inspect that the latest commit is the one you just pushed
- initiate a `merge request`: click `Merge requests` in the top menu and click the `New merge request` button
- select your repository as the source branch
- click on the `Select source branch` dropdown menu and select `main`
- select the original repository (i.e. `unigis/programming-for-gis`) as the target branch and click `Compare branches and continue`
- enter a short but/and descriptive title for this merge request
- click `Submit merge request`
- you have submitted a merge request... congratulations!

The tutors will inspect your proposed changes ASAP and merge them in the main repository (thereby updating the `students.csv` file there) or reject them and give you feedback on what/how to improve.

### Addendum: keep your fork in sync with its origin

The connection between a fork and its origin is static i.e. the fork is not automatically updated when the originating repository is updated. In other words, if this repository (i.e. `unigis/programming-for-gis`) is updated your fork at `https://gitlab.com/<your_username>/programming-for-gis` will remain unchanged.

This makes sense as it is often times undesirable and impossible to automatically pull in new code from your fork's origin. The code in the fork may may have e.g. diverged (i.e. become incompatible) with its origin, the fork might have a different branch structure, etc.

Synchronising is a manual process in which the repository on your machine plays a central role: it forms the link between your fork and its origin. To synchronise them you need to

1. pull the changes from your fork's origin (in this case `unigis/programming-for-gis`) to your machine
2. merge the changes with your local work
3. push the changes to your fork on GitLab (i.e `<your_username>/programming-for-gis`)

This dance is made possible by _Git's support for pulling code from more than one remote_. In other words, you can tell your cloned repository on your machine to pull code from both `unigis/programming-for-gis` as well as `<your_username>/programming-for-gis` and push to either one.

You can register new remotes with the `git remote add` command as follows

```bash
git remote add programming-for-gis https://gitlab.com/unigis/programming-for-gis.git
```

where `programming-for-gis` is the name of the remote you will use in your `pull` and `push` commands. A remote needs to be registered only once. The remote listing should reflect the addition of the new remote

```
git remote -v

programming-for-gis    https://gitlab.com/unigis/programming-for-gis.git (fetch)
programming-for-gis    https://gitlab.com/unigis/programming-for-gis.git (push)
origin  https://gitlab.com/<your_username>/programming-for-gis.git (fetch)
origin  https://gitlab.com/<your_username>/programming-for-gis.git (push)
```

You can now `pull` from `programming-for-gis` with `git pull programming-for-gis main`.

The complete pull, merge and push synchronization cycle is executed as

```bash
# pull new changes from your fork's origin on GitLab
git pull programming-for-gis main

# fix merge conflicts as needed
# add and commit fixed conflicts

# push changes to your fork on GitLab
git push origin
```

Note: `git pull` might fail if you have uncommitted work in the repository on your machine. You can either commit your work or [stash](https://git-scm.com/docs/git-stash) it, pull in the changes and unstash it.

You are advised to keep track of the [commit history of `unigis/programming-for-gis`](https://gitlab.com/unigis/programming-for-gis/commits/main) and pull in new changes regularly.

## Reflection

At this point you might be wondering why we went trough all this trouble to add one line of text to a simple text file. Would it not have been much easier to achieve this through e.g. a short email exchange? This is a valid question and the answer is: yes, for an extremely simple case as this email might be a better solution.

Consider, for example, a large(r) project in which a number of collaborators work in parallel on a code base consisting of several files. Combining their work without a proper versioning system will be extremely cumbersome as they would either have to manually merge their additions or design a process in which only one person can edit a certain file at any given time. Even if they managed to do this, they still lack a mechanism for tracking and logging changes as well as a clean and structured way to discuss these.

In the non-collaborative case (i.e. you are working on your own) even a small project will quickly consist of several files and many lines of code. Keeping track of changes, additions and improvements - especially when modifying existing code - quickly becomes a challenge. Git allows you to stay organised and keep a clean working environment.

### Summary of Git's benefits

While Git has a steep learning curve and adds some overhead to your workflow the benefits it brings to the table are significant.

Commits are in essence snapshots of your code at different moments in time. Combined with branches they enable

- "track changes" functionality: by comparing commits with `git diff` you can easily see what has changed between consecutive commits and coding sessions.
- a changelog: each commit's message captures the effect of a commit in plain language. Combined, commit messages form a changelog: what has happened when (and why).
- a backup of your codebase: Git stores all commits indefinitely and allows you to go back and forth between them. You can therefore never lose code you have committed. This is useful when e.g. modifying/improving existing code as you can simply throw old code away and write a fresh replacement knowing that you can always get the previous version when needed.
- a clean working environment: branches allow you to explore new development paths without having to comment out or duplicate code. They allow you to create a clean working environment that holds only the code you need at that moment and nothing more.
- reflection and introspection: the act of committing and crafting commit messages forces you to organise and reason about your code on a higher level of abstraction ang keep the bigger picture in mind.

Git helps you to organise your development process and taming your development process

### Summary of GitLab's benefits

GitLab might seem daunting at first and unnecessary if you are working alone but we believe that it too, just like Git, brings a considerable number of benefits to your workflow

- off-site backup: GitLab/Hub acts as an off-site backup location for your projects. This is useful in case your machine breaks or is stolen. `git clone` your code from any machine/service and start coding from any location/machine.
- effortless sharing of code: GitLab/Hub are geared towards frictionless sharing of code with people and online services. Simply point a colleague to your GitLab repo and they will have access to the latest version of your ode and data, and an insight in your development process.
- frictionless collaboration: receive code from others through GitLab's `Merge request` functionality.
- focused issue/project discussion: the issue tracker allows for extremely focused discussions about various aspects of your project.
- transparency and traceability - the commit log keeps track of who added/removed what pieces of code when and, more importantly, for what reason. Combined with a public issue tracker this results in unprecedented and automatic transparency of your code, data and decision making and development process.
- project management: the issue tracker can be used to manage projects by functioning as a TODO list. GitLab's Board function is a recent addition that helps you apply the Agile methodology to your projects.
- pleasant visualisation of diffs and branches: GitLab/Hub do a fairly decent job of displaying diffs, logs and branches.
