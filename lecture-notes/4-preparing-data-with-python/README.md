# Chapter 4. Preparing data with Python

In this module you will learn how to utilize python for data cleaning operations.

## Rationale
Data cleaning is a crucial step in the data analysis pipeline as the insights and results you produce is only as good as the data you have. As the old adage goes — garbage in, garbage out.

According the [Wikipedia](https://en.wikipedia.org/wiki/Data_cleansing), Data Cleaning is:

the process of detecting and correcting (or removing) corrupt or inaccurate records from a record set, table, or database and refers to identifying incomplete, incorrect, inaccurate or irrelevant parts of the data and then replacing, modifying, or deleting the dirty or coarse data.

Using dirty data to produce analysis will result in erroneous predictions that engenders bad decisions and dangerous outcomes. Not only that, most machine learning algorithms only work when your data is properly cleaned and fit for modeling.

## Outcomes

- be able to explore datasets with python.
- use packages as Pandas for data cleaning operations.
- make choices in data cleaning based on the analysis at hand.

## Resources

### Course


| Course type | Course                                                                  | Relevant chapters                         | Remarks                                                                                                                             |
| ----------- | ----------------------------------------------------------------------- | ---------------------------------------------: | ------------------------------------------------------------------------------------------------------------ |
| Website     | [Learn Python](https://www.learnpython.org/)                            |Data Science Tutorials (NumPy and Pandas) | Best suited for students who are just starting with programming and prefer a step-by-step interactive course |
| Book        | [The Coder's Apprentice](http://www.spronck.net/pythonbook/index.xhtml) | 26     | Best suited for students with no prior knowledge who prefer text-based study (available in english and dutch).                                |
| Website     | [RealPython](https://realpython.com/python-data-cleaning-numpy-pandas/) | all chapters  |  Website with in-depth tutorials for all levels of Python users.


### Reference

- [Pandas user guide](https://pandas.pydata.org/docs/user_guide/index.html) on pandas.pydata.org

### Lecture notes

In the remainder of this chapter, we will look at some data cleaning steps you can take based on a subset (100 features) of the polling station dataset.

### Using modules and load the dataset
Within this example the following modules will be used:
- **geopandas:** *conda install --channel conda-forge geopandas* 
- **numpy:** *conda install -c conda-forge numpy*
- **seaborn:** *conda install -c conda-forge seaborn*
- **matplotlib:** *conda install -c conda-forge matplotlib*

To install these modules, you can follow the [geopandas install guide](https://geopandas.org/en/stable/getting_started/install.html). For this course it's recomended to use the conda-forge channel.

We will import them as follows:
```python
import geopandas as gpd
import numpy as np
import seaborn as sns
import matplotlib.pyplot as plt
```
To load our dataset in Python we can utilize geopandas. This will create a spatially enabled (pandas) dataframe on which we can execute all of panda's functions and execute spatial operations.

```python
raw_df = gpd.read_file("./data/tk2021_subset.geojson")
```

### 4 steps of data cleaning

In this example we will take the following 4 data cleaning steps:
1. Search for missing data
2. Search for outliers
3. Clean unnecessary data
4. Check for inconsistencies

For each step we will discuss how we could clean the data.

#### Search for missing data
Data cleaning in Python often starts with identifying missing data. This step is almost never skipped. You can approach the identification of missing data in different ways.

```python
# Use a heatmap
plt.figure(figsize=(15,6))
cols = raw_df.columns
sns.heatmap(raw_df[cols].isnull(), cmap="YlGnBu",
            cbar_kws={'label': 'Missing data'})
```
![](images/heatmap.png)
We learn from the heatmap above that there are missing values in this dataset. We are missing values in the results of small political parties.

We can also summarize the number of missing values in the following way.
```python
# missing values per column
for column in raw_df.columns:
    amount_missing = np.sum(raw_df[column].isnull())
    if amount_missing > 0:
        print('{} - {}'.format(column, amount_missing))
```
```
JEZUS LEEFT - 15
Trots op Nederland (TROTS) - 27
U-Buntu Connected Front - 35
Blanco (Zeven, A.J.L.B.) - 33
Partij van de Eenheid - 61
DE FEESTPARTIJ (DFP) - 46
Vrij en Sociaal Nederland - 67
De Groenen - 91
Partij voor de Republiek - 91
Wij zijn Nederland - 64
Modern Nederland - 85
```
This gives us a list of political parties with missing values.

##### Clean missing values
There are several ways to clean missing data in your dataset:
- delete rows
- delete columns
- replace values

Depending on your data and the analysis at hand, you will make a choice of how you want to handle missing data.

```Delete rows```
```python
# Delete rows where data of a specified column is null
print('before', raw_df.shape)
missing = raw_df[raw_df['Modern Nederland'].isnull()].index
df_without_missing = raw_df.drop(missing, axis=0)
print('after', df_without_missing.shape)
```
```
before (100, 62)
after (15, 62)
```
By executing the delete rows script from above, only 15 of the 100 rows are left in the result dataframe. Is this what we want?

```Delete columns```
```python
# Delete columns
print('before', raw_df.shape)
df_del_column = raw_df.drop(['Modern Nederland'], axis=1)
print('after', df_del_column.shape)
```
```
before (100, 62)
after (100, 61)
```
By executing the delete column script from above, only 61 of the 62 columns are left in the result dataframe. Is this what we want?

```Replace null```
```python
# replace missing values with 0
print('before', raw_df['Modern Nederland'].unique())
raw_df['Modern Nederland'] = raw_df['Modern Nederland'].fillna(0)
print('after', raw_df['Modern Nederland'].unique())
```
```
before [nan  1.  0.]
after [0. 1.]
```
By executing the fillna script from above, the resulting dataset has two unique values (0 & 1). nan is removed from the dataframe. Is this what we want?

#### Search for outliers
Data cleaning in Python involves looking for outliers. Outliers are observations that are significantly different from other observations. An outliers can indicate an error in the dataset, but it can also be an actual value that is different. It is then up to the data scientist to determine whether the value remains included in the data set.

There are several ways to discover outliers when cleaning data. The most suitable way differs for numerical and categorical features. We discuss various methods.

```Descriptive statistics```

Descriptive statistics are a good way to identify outliers for numerical features. With .describe() you obtain different descriptive information per feature with regard to the data.

```python
print(raw_df.describe())
```
Here we can directly make the following observation:

- "Geldige stemmen" has a min of 35. 
- "Geldige stemmen" has a max of 4151.

4151 compared to 35 seems a very big difference. Is this correct?

To help us with this question, we can visualize this column in a histogram.
```python
raw_df['Geldige stemmen'].plot(kind='hist')
```
![](images/hist.png)

```Categorical data```
For a categorical variable it is best to use a bar chart for visualization.
```python
raw_df['election'].value_counts().plot(kind='bar')
```
![](images/cat.png)

Most of the records in the dataframe use ```TK2021```, but some also use ```TK3/17``` and ```TK17/0```. Is this correct?

#### Clean unnecessary data
The next step in Python data cleaning can be to see which data is unnecessary. Again, you can do this in several ways. For example, you can investigate whether:

- There are rows with almost only the same value. Logically, if there is hardly any variation in a feature, the predictive value also decreases.
- Irrelevant features are included in the dataset. For example, in our dataset, we have the type of election (```electionName```). Irrelevant features can be removed.
- There are duplications in the dataset. Many datasets have a unique feature (for example customer id) that makes it easy to deduplicate on the basis of one column. We don't have such a feature in this dataset, so this is more difficult (but it can be based on multiple columns like: ```gmcode``` & ```Stembureau```).

#### Check for inconsistencies
Finally, it is always good to ask yourself during data cleaning whether there are inconsistencies in the dataset. Several common inconsistencies include:

- Python is case sensitive and therefore inconsistencies in the use of capital letters can cause problems. This can be easily solved with .str.lower() for features that consist of text.
- Columns containing dates or times are often stored as data type 'string'. These are best converted to DateTime format with pd.to_datetime()
- Within categorical variables, typos can occur if the input is not standardized. There are several ways to spot typos. The most logical is to see which entries are very similar, this can be done with Fuzzy logic.

With geopandas we can also check for inconsistent geometries. For example:
- Are any geometries invalid?
- Are all geometries within the extend of the analysis at hand (all geometries within the extend of The Netherlands)?
- Any inconsistencies with the coordinate reference system?

See the [geopandas doc](https://geopandas.org/en/stable/docs/reference/geoseries.html) for functions you could use to check and clean your spatial data.

### Write to a new dataset
After cleaning your data, it is possible to directly pass the dataframe to your analysis pipeline. But in case of large processing time for the data cleaning, it might be beneficial to save your cleaned dataset to a new file. Then you can develop/use your analysis pipeline without, with each edit or possible crashes, waiting for your data cleaning pipeline. 

Saving dataframes with (geo)pandas is quite easy. If we want to create a new geojson file, we can simply run:

```python
clean_df.to_file('clean.geojson', driver="GeoJSON")
```
This will create a geojson file in the same folder as your python script.

## Exercise
Use the examples from this chapter to explore a dataset you have used before (during your work or your study). Are there any cleanup actions you need to perform?

Look at the examples from: [w3schools](https://www.w3schools.com/python/pandas/pandas_cleaning.asp)
