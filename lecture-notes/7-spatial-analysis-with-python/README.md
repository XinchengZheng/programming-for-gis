# Chapter 7. Spatial analysis with Python

In this chapter you will learn (by doing!) how to read spatial data stored in a Shapefile, transform it to something Python can understand, perform some basic calculations and analysis, and store the results in a new GeoJSON file which you can open in QGIS and [geojsonlint.com](https://geojsonlint.com/)

This chapter puts the knowledge you gained in the previous chapters into practice and reiterates in a step-by-step manner how to debug and lookup information "on the fly".

# Rationale

Python's spatial analysis capabilities are second to none. Packages such as Shapely, PySAL, GeoPandas and the QGIS and ArcGIS Application Programming Interfaces give you access to a large amount of geospatial processing power. Before you can use these modules, however, you need to get geospatial data in Python and transform it in something a module can work with: every spatial analysis starts with loading data from a file, database, etc. manipulating/analysing it and writing the results to a file, database, etc. Reading, writing, transforming and manipulating data are therefore essential skills to master in addition to data analysis.

In this chapter you will learn how to use modules to read/write files and perform simple spatial analysis using Shapely. These are kept simple on purpose to allow you to grasp the basics. If you understand how to operate on a single file you can read/write a hundred. The same goes for modules: grasping the basics of importing a module, studying its documentation ("on the fly") and using it effectively will allow you to work with most of them.

# Outcomes

- gain hands-on experience with third-party Python modules
- ability to read spatial data from files
- ability to perform basic spatial calculations and analyses with Shapely
- ability to transform data to GeoJSON and write it to a file

# Resources

## Lecture notes

A typical Python geospatial analysis pipeline closely resembles a typical QGIS/ArcGIS workflow:

1. read data from one or more files or a database
2. filter and transform the data into something Python can understand
3. perform analysis using one or more geospatial analysis packages/modules
4. filter and transform results in preparation for storage
5. write results to one or more files or a database
6. share data online

The challenges, on a general level, involved in implementing any workflow in Python are

1. determine which built-in Python functions or modules are needed for the task at hand
2. determine which module methods are useful and how to access and use them
3. determine how to link built-in functions and module methods to each other. What input does a certain operation expect, what output does it generate and what transformation do you need to apply to pass it on to the next method/function?

Addressing these challenges involves, initially at least, a fair bit of Googling, a semi-structured reading of the documentation and a lot of experimenting and tinkering. You might be tempted to start by reading the documentation from A to Z before writing any code. While this sounds like a robust and "correct" strategy, the reality is that

1. it is not feasible to read/learn everything prior to using it as it is simply too much
2. you will almost always run into undocumented/unexpected issues while coding regardless how hard you studied the docs

Hence, a better and may be more intuitive strategy is to start coding and look for information on a need-to-know basis i.e. use Google/the docs "on the fly" to find specific information only as required by an immediate problem you are facing. You are, however, still encouraged to scan the docs of a new module (as outlined in Chapter 3) in order to you have a general idea of what it can do, where to find each component and its documentation and, most importantly, which language to use when looking for information.

Let us set up a simple analysis pipeline to demonstrate how this strategy works in practice. In doing so we will implement some of the more theoretical parts of Chapter 2 and 3. Along the way you will see that in programming there are often more than one ways to solve a problem.

### Calculate the total number of inhabitants

Suppose we want to calculate the total number of inhabitants in the cities listed in `data/populations.csv` in this chapter. This is a Comma Separated Values files that consists of four columns: `city`, `lat`, `lon` and `population`.

To accomplish this task we need to 1) read the file in Python, 2) extract the population of each city and 3) somehow sum the three values to produce a grand total. Or, written in a more script-like manner, we need to

```
read the text file

for each line in the file
    extract the value in the population column
    add it to the previously extracted value

print the total
```

Let us start at the top, work our way down the recipe and deal with any problem/challenges we encounter as we encounter them.

You are encouraged to follow along: navigate to the `src/` folder and open `sum_populations.py` and copy the code snippets you encounter below by hand. Execute the script after every new addition or change.

The first step is to open the data file located in `data/populations.csv`. As you saw in `6_files.py` in Chapter 2's recap script the built-in `open()` function reads a file and returns an collection-like object we can iterate over in a loop. How do we discover the `open()` function in the first place? We can either 1) check Python's built-in functions since it is reasonable to expect that each Python has a mechanism for reading and writing files or 2) Google for `python open text files`.

```python
input_file = open('../data/populations.csv', 'r')

for line in input_file:
    print(line)
```

In the first line we open `populations.csv` for reading by calling `open()` and supplying 1) the path to the file we want to read/write and 2) the mode. `r` stands for read only, `w` for writing (an existing file will be deleted) and `a` for append. The `open` function returns a Python object that resembles a collection of the file's lines as strings. On line 4 we loop through the collection and print each string to the terminal.

```
city,lat,lon,population

Amsterdam,52.374,4.758,813562

Paris,48.858,2.277,2241346

Stockholm,59.326,17.701,1400000
```

Great, we have opened a file and read its contents in Python. We have to get rid of the header, however, as it is not part of the data. How do we do this? We can Google it but an easier (and useful to know) approach in this case is to use the `dir()` function we saw in Chapter 3. As you remember it displays all methods of a Python entity. Let us call it on the `input_file` entity

```python
input_file = open('../data/populations.csv', 'r')

print(dir(input_file))
```

Results in

```
['_CHUNK_SIZE', '__class__', '__del__', '__delattr__', '__dict__', '__dir__', '__doc__', '__enter__', '__eq__', '__exit__', '__format__', '__ge__', '__getattribute__', '__gt__', '__hash__', '__init__', '__init_subclass__', '__iter__', '__le__', '__lt__', '__ne__', '__new__', '__next__', '__reduce__', '__reduce_ex__', '__repr__', '__setattr__', '__sizeof__', '__str__', '__subclasshook__', '_checkClosed', '_checkReadable', '_checkSeekable', '_checkWritable', '_finalizing', 'buffer', 'close', 'closed', 'detach', 'encoding', 'errors', 'fileno', 'flush', 'isatty', 'line_buffering', 'mode', 'name', 'newlines', 'read', 'readable', 'readline', 'readlines', 'reconfigure', 'seek', 'seekable', 'tell', 'truncate', 'writable', 'write', 'write_through', 'writelines']
```

Scanning all elements without an underscore we see two potential candidates for skipping a line in a file: `next` and `readline` (as opposed to `readlines`). `next` moves to the next line in a file without returning it whereas `readline` does the same but returns the line so it can be stored in a variable. We can use either one as we do not want to store the header for later use

```python
input_file = open('../data/populations.csv', 'r')

input_file.readline()

for line in input_file:
    print(line)
```

Results in

```
Amsterdam,52.374,4.758,813562

Paris,48.858,2.277,2241346

Stockholm,59.326,17.701,1400000
```

The next step is to somehow split the population from a city's name so we can use it in our calculations.

How do we do that? Again, Google is our friend: searching for `python split text` leads us the `split()` method that, given a split character, chops a string into substrings at the location of the split character and returns them in a list. An alternative approach for finding this information is to open Python's documentation and inspect/scan the [string methods](https://docs.python.org/3/library/stdtypes.html#string-methods) section. This approach is useful if you are not sure what to Google for.

Implementing `split()` in our code gives us

```python
input_file = open('../data/populations.csv', 'r')
input_file.readline()

for line in input_file:
    print(line.split(','))
```

This results in

```
['Amsterdam', '52.374', '4.758', '813562\n']
['Paris', '48.858', '2.277', '2241346\n']
['Stockholm', '59.326', '17.701', '1400000\n']
```

In other words, `split(',')` reads a string and looks for `,`. When it finds one, it chops the substring that came before it and saves it in a list. It does this on every encounter of `,` until it reaches the end of the line.

The result is three lists filled with four elements: a city's name, its coordinates and its population. There is, however, something amiss with the population: it is trailed by a strange `\n` character which is not visible in the original file. This is the [newline](https://en.wikipedia.org/wiki/Newline) character that, as the name implies, signifies the end of a line in a text file. It is invisible in text editors as they replace it with the equivalent of the `Return` key on your keyboard.

Special characters such as `\n` are a source of nuisance that is difficult to debug since sometimes they are visible and other times they are not. Note, for instance, how it was rendered as `\n` when we printed a list but came out as a blank line when we printed it as a string. Let us remove it using the [rstrip()](https://docs.python.org/3/library/stdtypes.html?highlight=rstrip#str.rstrip) string method that we find by Googling for `python remove \n from string`.

`rstrip()` removes characters from the end of the string it is called on. For instance, `rstrip('!')` removes every `!` from the right side of the string. By default (i.e. when you do not specify a character explicitly) it removes `newline` character(s).

```python
input_file = open('../data/populations.csv', 'r')
input_file.readline()

for line in input_file:
    print(line.split(',')[3].rstrip())
```

Results in

```
813562
2241346
1400000
```

Looks good! Note how the blank line between each number has vanished.

The last line does a lot of things at once. Let us dissect it to see exactly what is going on

1. `line.split(',')` - splits the line at every `,` it encounters and puts the result in a list.
2. `line.split(',')[3]` - returns the fourth element of the list. In our case this is a string representation of the population.
3. `line.split(',')[3].rstrip()` - we apply the `rstrip()` function to remove the `\n` character.

You can store each operation's result in a variable and inspect it by calling `print` and `type`. `type()` is a [built-in Python function](https://docs.python.org/3/library/functions.html) that returns the type of a Python entity. Like `dir()` it is a useful instrument for exploring new and unknown (to you) Python entities.

```python
data = line.split(',')
print(type(data))

population_raw = data[3]
print(type(population_raw))

population = population_raw.rstrip()
```

The `type(data)` statement on the second line returns `<class 'list'>` i.e. the `data` variable stores a list. We can access its elements through the `data[n]` notation, see line 4. Calling `type(population_raw)` returns `<class 'str'>`. We can therefore call any of Python's string manipulation functions on it, `rstrip()` being one of them.

It seems we are ready to sum the populations. There are (at least) two approaches we can take: 1) put each value in a list using `.append()` and use the built-in function `sum()` to sum all elements of the list or 2) find the sum by adding each value to a `total_population` variable like so

```python
input_file = open('../data/populations.csv', 'r')
input_file.readline()

total_population = 0
for line in input_file:
    population = line.split(',')[3].rstrip()
    total_population = total_population + population

print(total_population)
```

This results in a `TypeError: unsupported operand type(s) for +: 'int' and 'str'`. Oh-oh. Something is the matter with the addition. The error message tells us that Python cannot add an integer to a string.

Looking at our code we can deduce that `total_population` is an integer since its initial value is `0`. `population` is, as we have already seen, a string. You can use the built-in function `type()` to verify your assumptions/deductions: it returns the type of a Python variable or entity. Calling `type(population)` for instance will return `<class 'str'>`

To solve this problem we need to transform the string to an integer. Googling for `python transform string to int` teaches us that such a transformation is called a _cast_ and that we can (again) a built-in function, namely `int()`. `int()` _casts_ a Python type into an integer like so

```python
input_file = open('../data/populations.csv', 'r')
input_file.readline()

total_population = 0
for line in input_file:
    population = int(line.split(',')[3].rstrip())
    total_population = total_population + population

print(total_population)
```

This results in `4454908`. Awesome!

Up to this point we have worked with Python's built-in capabilities. Next, we will see how to bring in an external module and feed it data from our data file.

### Reading data into Shapely

Suppose we also need to extract the coordinates of each city so we can manipulate them in Shapely. How would we go about doing that?

We saw in Chapter 3 how to create a Shapely polygon; creating a point is probably done in the same manner. A look at the [Points documentation](https://shapely.readthedocs.io/en/latest/manual.html#points) reveals that, indeed, points are created by calling `Point()` and supplying a `(x,y)` coordinate pair. Let's extract the x and y coordinates from our data file and create a Shapely point

```python
from shapely.geometry import Point

input_file = open('../data/populations.csv', 'r')
input_file.readline()

total_population = 0
for line in input_file:
    data = line.split(',')

    x_coordinate = data[1]
    y_coordinate = data[2]

    population = int(data[3].rstrip())
    total_population = total_population + population

    city_location = Point(x_coordinate, y_coordinate)
    print(city_location)

print(total_population)
```

We start by importing the `Point` entity from the `shapely.geometry` module. On line 7 we declare a `data` variable that holds the splitted line in the form of a list with four members. On line 9 and 10 we extract the x and y coordinate from `data`. On line 15 we create a Shapely point by calling `Point()` and supplying `x_coordinate` and `y_coordinate`. Running this script results in a `TypeError`

```
Traceback (most recent call last):

  File "C:\Users\peter\Desktop\UNIGIS - InternetGIS\testingch4.py", line 23, in <module>
    city_location = Point(x_coordinate, y_coordinate)

  File "C:\Users\peter\anaconda3\lib\site-packages\shapely\geometry\point.py", line 49, in __init__
    self._set_coords(*args)

  File "C:\Users\peter\anaconda3\lib\site-packages\shapely\geometry\point.py", line 132, in _set_coords
    self._geom, self._ndim = geos_point_from_py(tuple(args))

  File "C:\Users\peter\anaconda3\lib\site-packages\shapely\geometry\point.py", line 209, in geos_point_from_py
    dx = c_double(coords[0])

TypeError: must be real number, not str
```

You are looking at a so-called _Traceback_: a listing of all functions that were called before the emergence of an error. A _Traceback_ allows you to trace (back) the source of an error as, as you will see in a moment, the cause of an error need not lie close to it.

The message is clear: `a real number is required`. The question is: who, what and where requires a `real number` of what? A `TypeError` signifies a mismatch between the type of the expected and provided input. In other words, a function in Shapely expects a `float` while we are giving it something else. Let us find out where we are supplying faulty parameters.

On the _bottom_ of the _Traceback_ you can see the function that was executed **last** and triggered the error: `c_double(coords[0])`. Going up the trace we see the functions that were called before it: `geos_point_from_py(tuple)args))`, `_set_coords(*args)` and `Point(x_coordinate, y_coordinate)`. The task at hand is to keep reading the `Stacktrace` backwards until we encounter code that _we_ wrote since that is probably where we have provided the wrong input parameters. All the way on the top of the `Stacktrace` we see our call `Point(x_coordinate, y_coordinate)` (we know it ours since it is located in `sum_populations.py`).

Let us check the type of e.g. `x_coordinate` by adding a `print type(x_coordinate)` statement in the loop like so

```python
from shapely.geometry import Point

input_file = open('../data/populations.csv', 'r')
input_file.readline()

total_population = 0
for line in input_file:
    data = line.split(',')

    x_coordinate = data[1]
    y_coordinate = data[2]

    print(type(x_coordinate))

    population = int(data[3].rstrip())
    total_population = total_population + population

    # city_location = Point(x_coordinate, y_coordinate)
    # print(city_location)

# print(total_population)
```

This results in

```
<class 'str'>
<class 'str'>
<class 'str'>
```

Aha! `x_coordinate` is a string while `Point()` needs a float (per the error message). We need to, just like before, _cast_ the coordinates to `float`s. Googling for `python cast string to float` teaches us that the built-in function `float()` does what we need like so

```python
from shapely.geometry import Point

input_file = open('../data/populations.csv', 'r')
input_file.readline()

total_population = 0
for line in input_file:
    data = line.split(',')

    x_coordinate = float(data[1])
    y_coordinate = float(data[2])

    population = int(data[3].rstrip())
    total_population = total_population + population

    city_location = Point(x_coordinate, y_coordinate)
    print(city_location)

print(total_population)
```

This results in

```
POINT (52.374 4.758)
POINT (48.858 2.277)
POINT (59.326 17.701)
4454908
```

Great! We have successfully constructed three Shapely points. By calling `dir(city_location)` we can see what Shapely points can do

```
['__and__', '__array_interface__', '__class__', '__del__', '__delattr__', '__dict__', '__dir__', '__doc__', '__eq__', '__format__', '__ge__', '__geo_interface__', '__geom__', '__getattribute__', '__gt__', '__hash__', '__init__', '__init_subclass__', '__le__', '__lt__', '__module__', '__ne__', '__new__', '__or__', '__p__', '__reduce__', '__reduce_ex__', '__repr__', '__setattr__', '__setstate__', '__sizeof__', '__str__', '__sub__', '__subclasshook__', '__weakref__', '__xor__', '_crs', '_ctypes_data', '_geom', '_get_coords', '_is_empty', '_lgeos', '_ndim', '_other_owned', '_repr_svg_', '_set_coords', 'almost_equals', 'area', 'array_interface', 'array_interface_base', 'boundary', 'bounds', 'buffer', 'centroid', 'contains', 'convex_hull', 'coords', 'covers', 'crosses', 'ctypes', 'difference', 'disjoint', 'distance', 'empty', 'envelope', 'equals', 'equals_exact', 'geom_type', 'geometryType', 'has_z', 'hausdorff_distance', 'impl', 'interpolate', 'intersection', 'intersects', 'is_closed', 'is_empty', 'is_ring', 'is_simple', 'is_valid', 'length', 'minimum_rotated_rectangle', 'overlaps', 'project', 'relate', 'relate_pattern', 'representative_point', 'simplify', 'svg', 'symmetric_difference', 'to_wkb', 'to_wkt', 'touches', 'type', 'union', 'within', 'wkb', 'wkb_hex', 'wkt', 'x', 'xy', 'y', 'z']
```

That is a bunch of useful stuff! We can calculate a `buffer`, check if a point `intersects` another geometry, whether it is `within` another geometry, get its `coordinates`, etc.

We now have successfully read a file and transformed the data therein in "somethings Python understands" i.e. a Shapely `Point`.

### A simple data structure

Currently our script does not save our newly minted points. Let us fix this by creating the following data structure to hold each city's attributes.

```python
cities = {
    'Paris': {
        'population': 2241346,
        'location': city_location
    },
    'Stockholm': {},
    'Amsterdam': {}
}
```

In other words, we will extend the loop to crate a `cities` dictionary with keys equal to the cities' names and values equal to a dictionary containing their population and location as Shapely Point object.

```python
from shapely.geometry import Point, Polygon

input_file = open('../data/populations.csv', 'r')
input_file.readline()

cities = {}
total_population = 0
for line in input_file:
    data = line.split(',')

    x_coordinate = float(data[1])
    y_coordinate = float(data[2])

    population = int(data[3].rstrip())
    total_population = total_population + population

    city_location = Point(x_coordinate, y_coordinate)

    cities[data[0]] = {
        'population': population,
        'location': city_location
    }

print(cities)

# retrieve a city's location
print(cities['Paris']['location'])

# retrieve a city's population
print(cities['Paris']['population'])
```

This results in

```
{'Amsterdam': {'population': 813562, 'location': <shapely.geometry.point.Point object at 0x00000239AADA0EB0>}, 'Paris': {'population': 2241346, 'location': <shapely.geometry.point.Point object at 0x00000239AADA0C70>}, 'Stockholm': {'population': 1400000, 'location': <shapely.geometry.point.Point object at 0x00000239AADA0460>}}
POINT (48.858 2.277)
2241346
```

Congratulations! You have created a simple processing pipeline that reads data from a data file.

While manually reading data illustrates the (general Python) issues and quirks you may encounter, it is not the most effective or robust way of reading CSV files. Just like there is almost always "an app for that" chances are fairly high that you will find a "Python module for that". Have a look at Python's built-in [csv](https://docs.python.org/3/library/csv.html). Data analysis modules such as [Pandas](https://pandas.pydata.org/) and [Agate](http://agate.readthedocs.io) come with CSV readers as well.

For geospatial data [GeoPandas](http://geopandas.org/) combines functionality of [Shapely](https://shapely.readthedocs.io/en/stable/index.html), [Fiona](https://fiona.readthedocs.io/en/latest/), and [Pandas](https://pandas.pydata.org/) to make an (arguably) easier to use python module to read/write spatial data, perform basic geometric operations and plot spatial data.

# Exercise

In this exercise you will set up your own analysis pipeline using the skills you learned in this chapter as well as Chapters 2 and 3.

## The task

The task at hand is to find the _geometries of all intersections_ in `data/areas.shp`, calculate their areas and perimeter, store them in a GeoJSON file and load them in QGIS to verify the results. In other words, we are interested in the geometries that form the intersections between the polygons in `areas.shp`, see Figure 1.

![](img/data.png)

Figure 1. The contents of `data/areas.shp`.

## Workflow

The analysis consists of three stages

1. read the geometries from `area.shp` and transform them into Shapely polygons
2. use Shapely's spatial analysis capabilities to find the intersections and generate their polygons, and calculate their areas and perimeter
3. write the results to a GeoJSON file and display in QGIS or

Following is a general suggestion about how to go about performing these tasks.

### 1. Read the data

The first step is to read `areas.shp`, extract the geometries and their coordinates and attributes. You will need to find a Python module (maybe your favourite from the previous chapter) that can read Shapefiles and find out how to extract

1. the geometries
2. each geometry's coordinates
3. each geometry's attributes

As you saw in the previous chapter (and in Shapely's documentation) you will need a geometry's coordinates to create Shapely polygons. Creating Shapely `Polygons` may then be achieved as follows

    for each shapefile geometry
        get its coordinates
        create Shapely Polygon
        store Polygon for later use

### 2. Analyse the data

Once you have ingested the geometries from the Shapefile and created Shapely `Polygon` objects you are ready to start looking for intersections. The general approach will look something like

    for each polygon
        if it intersects with any other polygon
            calculate the intersection's geometry
            calculate the intersection's area and perimeter
            store the geometry and its area and perimeter

It is up to you to choose which collection (list, list of lists, dictionary, etc.) to use to store the intersections, how to prevent intersections from being counted twice, etc.

### 3. Write the data

The task is to write the data to a GeoJSON file. GeoJSON is a web-friendly geodata standard that has gained in popularity in the last couple of years. Many web-based tools and platforms support it.

**Note**: read `Appendix A. Sharing data online: JSON and GeoJSON` (see below) before continuing.

GeoJSON files are text files: you can edit them by hand (hence their popularity), write them with Python's built-in file writing facilities or find a module that can do that for you. Since (Geo)JSON strongly resembles Python's dictionaries it is fairly straightforward to transform one into the other with e.g. Python's built-in `json` module: see its `dumps` and `loads` functions.

### (Optional, but recommended!) 4. Same workflow using GeoPandas

Use the earlier mentioned GeoPandas module to perform the same analysis. Compare the two modules and decide which of the two you like more. In general **GeoPandas should be easier**, but give you less freedom. (GeoPandas uses shapely under the hood)

## Appendix A. Sharing data online: JSON and GeoJSON

Most of the spatial data you will encounter will probably take the form of Shapefiles or Geometry Markup Language (GML) files. Although powerful, both formats are somewhat cumbersome to work with and not overly suited for publication and sharing on the web. As a result a bottom-up standard has emerged in recent years that has replaced Shapefiles and GML files as exchange formats on the web.

GeoJSON is a lightweight data-interchange standard based on the JavaScript Object Notation (JSON) format. JSON is a text format for encoding data (strings, integers, lists, dictionaries) that is easy to read and write by humans and machines. In recent years it has become the de-facto standard for data exchange on the web: most online Application Programming Interfaces and services "speak" JSON. Similarly, GeoJSON is slowly replacing Shapefile where possible as the go-to exchange format for geospatial data.

### JSON

JSON is built on two structures: _arrays_ and _objects_. An _array_ is an ordered list of _values_. An array begins with `[` and ends with `]`; values are separated by `,`, like so

```json
["Amsterdam", "Paris", "Stockholm"]
```

JSON _arrays_ strongly resemble Python lists.

An _object_ is a collection of _name_/_value_ pairs. An _object_ begins with `{` and ends with `}`. Each _name_ is enclosed in double quotes and is followed by `:`. Name/value pairs are separated by `,`.

```json
{
  "first_name": "Jane",
  "last_name": "Doe",
  "age": 32
}
```

As you can see JSON _objects_ strongly resemble Python dictionaries.

A _value_ in both cases can be:

- a string in double quotes,
- a number,
- `true`, `false` or `null`
- an _object_ or an _array_.

The last option is significant: it means we can embed _arrays_ and _objects_ in other _arrays_ and _objects_ thereby creating complex data structures like the following

```json
{
  "first_name": "Jane",
  "last_name": "Doe",
  "age": 32,
  "hobbies": ["hiking", "climbing", "kayaking"],
  "siblings": [
    {
      "first_name": "John",
      "last_name": "Doe",
      "age": 30
    },
    {
      "first_name": "Malcom",
      "last_name": "Doe",
      "age": 27
    }
  ]
}
```

JSON does not specify a data structure/model (i.e. the manner in which one should organise the data). It merely offers a syntax for capturing dictionaries, lists, strings and integers in a text-based format.

### GeoJSON

GeoJSON is a JSON-based standard for describing geospatial data. Contrary to JSON it does define a data structure i.e. every GeoJSON file describes points, lines, polygons and collections of geometries in the exact same manner (using JSON notation).

A point in GeoJSON is described as follows

```json
{
  "type": "Feature",
  "geometry": {
    "type": "Point",
    "coordinates": [125.6, 10.1]
  },
  "properties": {
    "name": "Dinagat Islands"
  }
}
```

The `type` key describes the GeoJSON object: `Feature` or `FeatureCollection` (see below). The `geometry` key describes the object's geometry. In this case its `type` is `Point` and has two `coordinates` defined in a list. The `properties` key contains the object's attribute(s).

Lines have a `type` equal to `LineString`; their `coordinates` are a list of `[lat, lon]` lists as follows

```json
{
  "type": "Feature",
  "properties": {},
  "geometry": {
    "type": "LineString",
    "coordinates": [
      [
        -13.359375,
        17.308687886770034
      ],
      [
        47.8125,
        57.326521225217064
      ],
      ...
    ]
  }
}
```

To visualise a GeoJSON snippet you can either

1. save it to a file with a `.geojson` extension and open it in QGIS
2. manually copy (as always) the snippet in [geojsonlint.com](https://geojsonlint.com/). You can use [geojsonlint.com](https://geojsonlint.com/) to practice and experiment with the JSON syntax and the GeoJSON structure.

You are encouraged to use QGIS, geojson.io or GeoJSON's documentation to study how to store polygons.

A collection of features is described in a `FeatureCollection` as follows:

```json
{
  "type": "FeatureCollection",
  "features": [
    {
      "type": "Feature",
      "geometry": {
        "type": "Point",
        "coordinates": [125.6, 10.1]
      },
      "properties": {
        "name": "Dinagat Islands"
      }
    },
    {
      "type": "Feature",
      "geometry": {
        "type": "Point",
        "coordinates": [0, 0]
      },
      "properties": {
        "name": "Null Island"
      }
    }
  ]
}
```

As you can see, a `FeatureCollection` is simply a JSON object with two keys: `type` and `features`. The features themselves are identical to the singular case.

See [Tom MacWright's extremely thorough and clear GeoJSON explainer](https://macwright.org/2015/03/23/geojson-second-bite.html#featurecollection) for more information.
