# TAA1 - Starting with Python (Std)
Deadline Feb 11

## Learning goals
- Install Python and use several functionalities of an Integrated development environment.
- Use the basic functionality of public/private collaboration platforms such as GitLab/GitHub, StackOverflow, GeoForum and others.

## Students will learn to: 
-	setup a Python environment.
-	use basic datatypes and functionality of the Python programming language.
-	read and debug code.

## Prerequisite:
- Read the course manual.
- Finish chapter 2: Python programming 1 – The basics

## Assignment description
In this assignment you will first setup your environment for the rest of the course. After this initial setup, the programming begins. Get acquainted with the basics of the Python programming language and utilize its different built-in datatypes and functionality.

Within the lecture notes of chapter 2 on git you will find Python resources and a step by step instruction on how to read-, use and debug code. Git is also the main communication platform during this course. When you get stuck during the lecture notes and/or the assignments, use the issue tracker in the course repository.

For this formative assignment you must submit proof of your work in a Python debugger within chapter 2.

# Deliverables:
-	A print screen of your work in a Python debugger during chapter 2.
