# TAA5 – Broaden your horizon: web applications (Std)
Deadline Apr 14

## Prerequisite:
- Finish chapter 8 – Geospatial on the web.

## Assignment description
During this course you have learned how to setup and utilize a python pipeline to collect- , clean data and perform a geospatial analysis. The resulting datasets could be used to create maps in a desktop GIS (e.g. ArcGIS and QGIS), but are also useful for mobile and web-based applications.

In this final assignment you will reflect on the usability of your analysis results in a geospatial web application. Search for (at least) 3 examples of interesting/inspiring geospatial webapps. Provide links to these examples and describe the following per application:
-	Why do you find the visualization and/or interactivity of the app interesting.
-	Which aspects of the app do you think are less useful, under developed, or missing?
-	By using the geospatial webapp as and example, describe how your calculated election results be used in a new application with similar functionality?

Post your findings on the issue tracker on GitLab.

# Deliverables:
-	A link to your post on GitLab.
