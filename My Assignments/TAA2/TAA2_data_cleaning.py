import json
import requests
import pandas as pd
import geopandas as gpd
import seaborn as sns
import matplotlib.pyplot as plt
import os
import numpy as np

# Download the election data via URL
def download_tk2021(url):
    response = requests.get(url, verify=False)
    
    with open("tk2021.geo.json", 'wb') as file:
        file.write(response.content)
    print(f"GeoJSON has been downloaded")

# Save cleaned data
def save_df(df, df_name):
    df.to_file(df_name, driver='GeoJSON')
    print("{df_name} has been saved!")

# Analyze null value and the amount of null value in each column
def null_value_analyze(election_df):
   for column in election_df.columns:
        amount_missing = np.sum(election_df[column].isnull())
        if amount_missing > 0:
            #print(election_df[column])
            print('{} - {} / {}'.format(column, amount_missing, '13824'))

# Replace all null values with zero
def replace_null_with_zero(election_df):

    election_df.fillna(0, inplace=True)

    return election_df

# Plot a heatmap
def heat_map(election_df, heatmap_name):

    # 绘制heatmap
    plt.figure(figsize=(10, 8))
    sns.heatmap(election_df.isnull(), cmap='YlGnBu', cbar_kws={'label': 'Missing data'})
    plt.title('Missing Values Heatmap')
    plt.savefig(heatmap_name, dpi=300)

def nested_interactive(election_df):
    while True:
        nested_choice = input("1. replace all null values with zero, 2. export a heatmap, or 0. Back：")
        if nested_choice == "1":
            replace_null_with_zero(election_df)
            print("All null values have been replaced!")

            heatmap_request = input("Do you like export a heatmap? 1. Yes 2. No: ")
            
            if heatmap_request == "1":
                heatmap_name = input("Input a name for the heatmap: ")
                heat_map(election_df, heatmap_name)
            else:
                break
            break
        elif nested_choice == "2":
            heatmap_name = input("Input a name for the heatmap: ")
            heat_map(election_df, heatmap_name)
        elif nested_choice == "0":
            break
        else:
            print("Invalid")

# Summary data
def describe_data(election_df):
    print(election_df.describe())

# Delete specific rows under a certain condition
def delete_rows_or_columns(df, column_name=None, threshold=None, row_index=None):
    if row_index is not None:
        df.drop(row_index, inplace=True)
        print(f"Row {row_index} has been deleted!")
    elif column_name is not None and threshold is not None:
        comparison = input("Select a condition（'>', '=', '<'）：")
        if comparison == '>':
            df = df[df[column_name] > threshold]
        elif comparison == '=':
            df = df[df[column_name] == threshold]
        elif comparison == '<':
            df = df[df[column_name] < threshold]
        else:
            print("Invalid!")
            return
        print(f"The row which {column_name}'s data {comparison} {threshold} has been deleted")
    else:
        print("Invalid!")

# Plot a hist or bar plot
def plot_hist(df, column, name, figure_kind):
    if figure_kind == "hist":
        df[column].plot(kind='hist')
    elif figure_kind == "bar":
        df[column].value_counts().plot(kind='bar')

    plt.savefig(name, dpi=300)

def interactive_delete(election_df):
    
    print("1. Draw a figure for more information")
    print("2. Delete rows with values greater than, equal to, or less than a specific value in a column")
    print("3. Delete a specific row directly")
    choice = input("What do you need?：")
    
    if choice == "1":
        column_name = input("Input the column: ")
        figure_kind = input("Select a kind (hist/bar): ")
        figure_name = input("Input a name for this figure: ")

        plot_hist(election_df, column_name, figure_name, figure_kind)
    elif choice == "2":
        column_name = input("Input the column：")
        threshold = float(input("Input the threshold："))
        delete_rows_or_columns(election_df, column_name=column_name, threshold=threshold)
    elif choice == "3":
        row_index = int(input("Input the row index："))
        delete_rows_or_columns(election_df, row_index=row_index)
    else:
        print("Invalid!")

#Delete duplicates directly
def delete_duplicates(df):
    df.drop_duplicates(inplace=True)
    print("Duplicates have been deleted!")

def interactive_delete_duplicate(election_df):
    while True:
        print("1. Delete duplicate rows")
        print("2. Delete specific row(s) or column(s)")
        print("3. Delete duplicates based on specific columns")
        print("4. Back")
        choice = input("Input your choice：")

        if choice == "1":
            delete_duplicates(election_df)
            break

        elif choice == "2":
            delete_choice = input("You want to delete 'row' or 'column'? or 'back'：")
            if delete_choice == "row":
                rows_to_delete = input("Please enter row(s) index, separate by space：").split()
                for row in rows_to_delete:
                    delete_rows_or_columns(election_df, row_index=int(row))
                    print(f"Row {row} has been deleted!")
                break

            elif delete_choice == "column":
                columns_to_delete = input("Please enter column(s) name, separate by space：").split()
                for col in columns_to_delete:
                    del election_df[col]
                    print(f"Column '{col}' has been deleted!")
                break

            elif delete_choice == "back":
                break

            else:
                print("Invalid!")

        elif choice == "3":
            columns_to_check = input("Please enter the columns to check for duplicates, separated by space: ").split()
            if all(col in election_df.columns for col in columns_to_check):
                election_df.drop_duplicates(subset=columns_to_check, inplace=True)
                print(f"Duplicates based on columns {', '.join(columns_to_check)} have been deleted!")
            else:
                print("One or more columns do not exist in the DataFrame. Please try again.")
            break

        elif choice == "4":
            break
        else:
            print("Invalid")

#Users can filter data as theirs goal
def filter_data(df):
    while True:
        filter_type = input("Choose filter type ('row' for rows, 'column' for columns, 'exit' to stop): ").strip().lower()
        if filter_type == 'exit':
            print("Exiting filter process.")
            break

        if filter_type not in ['row', 'column']:
            print("Invalid input. Please enter 'row', 'column', or 'exit'.")
            continue
        
        # Select a column for fitlering
        column_name = input("Enter the column name for filtering: ").strip()
        if column_name not in df.columns:
            print("Column name not found in the DataFrame. Please try again.")
            continue
        
        # Determine a threshold
        operation = input("Choose an operation ('>', '=', '<'): ").strip()
        if operation not in ['>', '=', '<']:
            print("Invalid operation. Please enter '>', '=', or '<'.")
            continue

        try:
            value = float(input("Enter the value to compare: ").strip())
        except ValueError:
            print("Invalid input. Please enter a numeric value.")
            continue

        if operation == '>':
            filtered_df = df[df[column_name] > value]
        elif operation == '=':
            filtered_df = df[df[column_name] == value]
        elif operation == '<':
            filtered_df = df[df[column_name] < value]

        print(f"Filtered {len(filtered_df)} rows based on your criteria.")

        while True:
            next_step = input("What next? ('continue' to keep filtering, 'save' to save, 'exit' to finish): ").strip().lower()
            if next_step == 'continue':
                df = filtered_df
                break
            
            # Save the filtered data to local and decide whether continue filter or not
            elif next_step == 'save':
                file_name = input("Enter the file name to save: ").strip()
                filtered_df.to_file(file_name, driver='GeoJSON')
                print(f"Filtered data has been saved to '{file_name}'")
                
                after_save = input("Do you want to continue filtering or exit? ('continue' to keep filtering, 'exit' to finish): ").strip().lower()
                if after_save == 'continue':
                    break
                elif after_save == 'exit':
                    print("Exiting filter process.")
                    return
                else:
                    print("Invalid input. Please enter 'continue' or 'exit'.")
            elif next_step == 'exit':
                print("Exiting filter process.")
                return
            else:
                print("Invalid input. Please enter 'continue', 'save', or 'exit'.")

# Check geometry type
def check_geometry_type(df, expected_type):
    
    if not all(df.geometry.type == expected_type):
        print(f"Some geometries are not of type {expected_type}.")
    else:
        print(f"All geometries are of type {expected_type}.")

# Check coordinates format
def check_coordinates_format(df):

    incorrect_coords = []
    for idx, geom in enumerate(df.geometry):
        if not geom.is_valid:
            incorrect_coords.append(idx)
    
    if incorrect_coords:
        print(f"Incorrect coordinate format found in geometries at indices: {incorrect_coords}")
    else:
        print("All coordinate formats are correct.")
    
    continue_check = input("Do you want to check if all coordinates are within the Netherlands? (yes/no): ").strip().lower()
    if continue_check == 'yes':
        check_coordinates_within_netherlands(df)

# Check whether coordinates within the Netherlands
def check_coordinates_within_netherlands(df):

    netherlands_bounds = (3.3594, 50.7504, 7.2275, 53.5552)  # (min_lon, min_lat, max_lon, max_lat)
    outside_bounds = []

    for idx, geom in enumerate(df.geometry):
        if not (netherlands_bounds[0] <= geom.x <= netherlands_bounds[2] and netherlands_bounds[1] <= geom.y <= netherlands_bounds[3]):
            outside_bounds.append(idx)
    if outside_bounds:
        print(f"Found coordinates outside the Netherlands at indices: {outside_bounds}")
    else:
        print("All coordinates are within the Netherlands.")

def interactive_geojson_checker(df):

    print("GeoJSON Checker")
    print("1. Check geometry type")
    print("2. Check coordinates format")
    choice = input("Enter your choice (1 or 2): ")

    if choice == '1':
        expected_type = input("Enter the expected geometry type (e.g., 'Point', 'Polygon'): ")
        check_geometry_type(df, expected_type)
    elif choice == '2':
        check_coordinates_format(df)
    else:
        print("Invalid choice. Please enter 1 or 2.") 

#Read the election data from URL
if not os.path.exists("tk2021.geo.json"):
    election_data_url = 'https://data.openstate.eu/dataset/b44d992e-0584-496b-a8d0-837f288a832c/resource/75f5b21a-69b2-4a9e-b80f-251d357dfe0b/download/tk2021.geo.json'

    download_tk2021(election_data_url)
else:
    print("tk2021.geo.json already exists")

election_df = gpd.read_file("tk2021.geo.json")

# Stembureau_1 = election_df[election_df['Stembureau'] == 1]
# print(Stembureau_1)

while True:
    print("\n")
    print("1. Search for missing data")
    print("2. Search for outliers")
    print("3. Filter")
    print("4. Clean unnecessary data")
    print("5. Check for inconsistencies")
    print("6. Save data")
    print("0. End this programm")
    print("Tips: You can always back here by input 'back'！")

    choice = input("Please tell me what to do next: ")

    if choice == "1":
        null_value_analyze(election_df)
        nested_interactive(election_df)

    elif choice == "2":
        describe_data(election_df)
        print("Please check data description above!")
        interactive_delete(election_df)

    elif choice == "3":
        filtered_df = filter_data(election_df)

    elif choice == "4":
        interactive_delete_duplicate(election_df)

    elif choice == "5":
        interactive_geojson_checker(election_df)

    elif choice == "6":
        file_name = input("Input the file name: ")
        save_df(election_df, file_name)

    elif choice == "0":
        print("End")
        break

    else:
        print("Invalid input")

    print("\n")





